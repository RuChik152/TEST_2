﻿Start-Process powershell -Verb runAs
set GIT_REDIRECT_STDERR=2>"&"1


$data = Read-Host "Укажите через пробел раширение которые нужно отслеживать Exempel: txt jpg"
$arr = $data.Split(" ")

set GIT_REDIRECT_STDERR=2>"&"1

Write-Host "-------Installing Git LFS--------" -ForegroundColor Cyan
Write-Host "================================="

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# delete existing Git LFS
del 'C:\Program Files\Git\mingw64\bin\git-lfs.exe' -ErrorAction SilentlyContinue

$exePath = "$env:TEMP\git-lfs-windows.exe"

Write-Host "Downloading......"
(New-Object Net.WebClient).DownloadFile('https://github.com/git-lfs/git-lfs/releases/download/v3.3.0/git-lfs-windows-v3.3.0.exe', $exePath)

Write-Host "Installing......"
cmd /c start /wait $exePath /VERYSILENT /SUPPRESSMSGBOXES /NORESTART

#Add-Path "$env:ProgramFiles\Git LFS"
#Add-SessionPath "$env:ProgramFiles\Git LFS"

git lfs install --force
git lfs version

Write-Host "-----Git LFS installed------"
Write-Host "============================"

Write-Host "----Settings Repo----"
Write-Host "====================="

$CurrentDir = $PSScriptRoot
$name = ".lfsconfig"

$path2 = $PSCommandPath


cd $CurrentDir
Remove-Item -Path "$CurrentDir\$name"
New-Item $name
Add-Content -Path .\.lfsconfig -Value "[lfs] `n `t url = `"http://95.79.25.159:9966`""
git lfs install

foreach($str in $arr) {
    git lfs track "*.$str"
}

git add .gitattributes
git add .lfsconfig

